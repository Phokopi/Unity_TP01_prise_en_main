﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManipulationMouse : MonoBehaviour
{
    private Vector3 screenSpace;
    //private Vector3 offset;
    private Vector3 curScreenSpace;
    private Vector3 curPosition;

    // Use this for initialization
    void Start()
    {
    }

    void OnMouseDown()
    {
        //translate the cubes position from the world to Screen Point
        screenSpace = Camera.main.WorldToScreenPoint(transform.position);

        //calculate any difference between the cubes world position and the mouses Screen position converted to a world point  
        //offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));

    }


    void OnMouseDrag()
    {
        //keep track of the mouse position
        curScreenSpace.Set(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);

        //convert the screen mouse position to world point and adjust with offset
        curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace);// + offset;

        //update the position of the object in the world
        transform.position = curPosition;
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Debug.Log("Mouse left button pressed !");
        }
    }
}
