﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitalCamera : MonoBehaviour {

    public Transform target;
    public float zoomSpeed = 0.1f;
    public float rotationSpeed = 1f;

    // Use this for initialization
    void Start () {
        // Make camera look at the center of our target
        transform.LookAt(target);
	}
	
	// Update is called once per frame
	void Update () {

        // move left
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.RotateAround(target.transform.position, Vector3.up, rotationSpeed);
        }

        // move right
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.RotateAround(target.transform.position, -Vector3.up, rotationSpeed);
        }

        // move up
        if (Input.GetKey(KeyCode.UpArrow)) // pour ne pas passer "en dessous" du plan
        {
            transform.RotateAround(target.transform.position, transform.right, rotationSpeed);

        }

        // move down
        if (Input.GetKey(KeyCode.DownArrow)) 
        {
            transform.RotateAround(target.transform.position, -transform.right, rotationSpeed);

        }

        // zoom in
        if (Input.GetKey(KeyCode.KeypadPlus) || Input.GetKey(KeyCode.Plus)) 
        
        {
            transform.position += (transform.forward).normalized * zoomSpeed;
        }

        // zoom out
        if (Input.GetKey(KeyCode.KeypadMinus) || Input.GetKey(KeyCode.Minus))
        {
            transform.position += -(transform.forward).normalized * zoomSpeed;
        }
 
    }
}
