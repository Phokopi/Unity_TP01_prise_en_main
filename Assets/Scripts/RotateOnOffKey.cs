﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnOffKey : MonoBehaviour {

    // rotation support
    private bool shouldRotate = false;

    public string keyToCheck = "r";
    public float speed = 5.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // check if we want to start/stop rotating
		if (Input.GetKeyDown(keyToCheck))
        {
            // if already rotating
            if (shouldRotate)
            {
                // tell to stop rotating
                shouldRotate = false;
            }
            else
            {
                // tell to start rotating
                shouldRotate = true;
            }
        }

        // rotate if needed
        if (shouldRotate)
        {
            transform.Rotate(0, speed, 0);
            Debug.Log("Debug log : key '" + keyToCheck + "' pressed");
        }
	}
}
