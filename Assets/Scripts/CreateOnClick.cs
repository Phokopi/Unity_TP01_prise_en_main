﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateOnClick : MonoBehaviour {
    
    // Click support
    private bool clickStarted = false;

    // What to copy and where
    public Transform prefab;
    public Transform whereToCreate;

    // to deal with the copy's renderer (materia and color)
    private MeshRenderer m_Renderer;


    // Use this for initialization
    void Start () {
        
    }
    
	
	// Update is called once per frame
	void Update () {
        // press right button down
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            clickStarted = true;
        }

        // right button up and was previously pressed
        if (Input.GetKeyUp(KeyCode.Mouse1) && clickStarted)
        {
            // click is over
            clickStarted = false;

            // debug message
            Debug.Log("Mouse right button clicked !");

            // create a new instance of the prefab, on top of the center of the target
            Transform newPrefab = Instantiate(prefab, whereToCreate.position + new Vector3(0, transform.lossyScale.y/2, 0), Quaternion.identity);
            newPrefab.GetComponent<MeshRenderer>().material.color = new Color(Random.value, Random.value, Random.value);
        }
    }
}
