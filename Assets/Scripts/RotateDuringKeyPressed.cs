﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateDuringKeyPressed : MonoBehaviour {

	public float speed = 5.0f;
	public string keyToCheck = "r";

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (keyToCheck)) {
			transform.Rotate (0, speed, 0);
			Debug.Log ("Debug log : key '"+ keyToCheck +"' pressed");
		}
	}
}
